var

canvas = document.getElementById('background'),
context= canvas.getContext('2d'),
score = 0,

musicElement = document.getElementById('song');

background = new Image();
spritesheet = new Image();


//canvas
canvas.width = 350;
canvas.height = 550;

//drummer
drummerSpritePosi = 90;
drummerHeight = 251;
drummerPrintWidth=170;
drummerPrintHeight=220;

drummerDownClipX=0;
drummerDownClipY=0;
drummerDownClipWidth=137;


drummerRightClipX=137;
drummerRightClipY=0;
drummerRightClipWidth=135;

drummerUpClipX=272;
drummerUpClipY=0;
drummerUpClipWidth=142;

drummerNeutralClipX=0;
drummerNeutralClipY=255;
drummerNeutralClipWidth=140;

drummerLeftClipX=144;
drummerLeftClipY=256;
drummerLeftClipWidth=131;


//circles
circleSpritePosi=0;
newCircleSpritePosi=0;

circleHeight=115;
circlePrintHeight=90;
circlePrintWidth=90;

circleshiftX = 0;
circleshiftY = 0;


circleClipX = 320;
circleClipY = 269;
circleClipWidth = 109;

count = 0;


direction = Math.random();

animateVelocity = 0.3;

rightcircleSpritePosi = canvas.width + 10;
downcircleSpritePosi = canvas.height + 10;

lives = 3;
drummerTouched = false;


failed = false;
paused = false;
var fps = 0;
var time;
var lasUpdate;
var drummer = null;

context.fillStyle = "blue";
context.font = "20px Arial ";

context.strokeStyle = 'blue';




function fpsCount() {

    let now = new Date();
    fps = 1000 / (now - lasUpdate);
    lasUpdate = now;

    document.getElementById('FPS').innerHTML = 'FPS: ' + Math.round(fps);

}

function pause() {

    paused = true;

}


function unpause() {

    paused = false;

}

function initializeImages() {

    //creation and settings of the game for the images and such

    if (paused === false) {

        spritesheet.src = "./images/sheet.png";
        
        
        background.src = "./images/background.png";
        spritesheet.onload = function () {
            startGame();
        }
    }

    else if (paused === true) {
        draw();
    }

}

function startGame() {


    animate();
    startMusic();
}

function animate(){
    animateStars();
}


function speedUp() {
    if (score >= 200) {
        animateVelocity = 0.5;
    }
    if (score >= 500) {
        animateVelocity= 1;
    }
    if (score >= 1000) {
        console.log("Why are you still here", animateVelocity);
        animateVelocity = 1.2;
    }

    //speeds up background, hearts and alien based on the points obtained in the game

}

//sets the boxes in which the player stars are stopped at
function checkSpawn(){
    context.strokeRect(drummerSpritePosi + 40, drummerHeight-120, 90,90 );
    context.strokeRect(drummerSpritePosi + 40, (drummerHeight+ drummerPrintHeight)-20 , 90,90);

    context.strokeRect(drummerSpritePosi - 85, drummerHeight+ 60, 90, 90);
    context.strokeRect(drummerSpritePosi + 165, drummerHeight+ 60, 90, 90);
}

function animateStars(){

    fpsCount();

    count ++;

    if(count == 100){
        count =0;
        drummer = null;
    }
        

    if (paused == false && failed == false) {

        context.clearRect(0, 0, canvas.width, canvas.height);
        drawBackground();
        drawDrummer();
        checkSpawn();

        //newCircleSpritePosi = circleSpritePosi + circleOffset;


       if (direction  <=0.25){
            circleshiftX = (canvas.width/2)-45;
            circleSpritePosi = -10;

            circleFromUp(circleshiftX, circleSpritePosi);
           
        }
       

        else if (direction >0.25 && direction <=0.5){
            circleshiftX = canvas.width;
            circleshiftY = canvas.height/2 + 70;

            
            circleSpritePosi = 10;
            circleFromRight(circleshiftY, circleSpritePosi);
            
        }
        

        else if (direction >0.5 && direction <=0.75){
            circleshiftX = (canvas.width/2)-45;
            circleshiftY = canvas.height;

            circleSpritePosi= canvas.height +15;
            circleFromDown(circleshiftX, circleSpritePosi);
            
        }
 

        else if (direction >0.75 && direction <=1){
            circleshiftY = canvas.height/2 + 70;

            circleSpritePosi = -10;
            circleFromLeft(circleshiftY, circleSpritePosi);
            
        }


        requestAnimationFrame(animateStars);

        detectBeatTouch();

    }


    else if (paused == true) {

        context.fillText("Paused", (canvas.width / 2 - 30), canvas.height / 2);

    }

    if(lives == 0){
        gameOver();
    }


}

function circleFromUp(circleshiftX, circleSpritePosi){

    
    newCircleSpritePosi = newCircleSpritePosi - circleSpritePosi *(animateVelocity);
    context.drawImage(spritesheet, circleClipX, circleClipY, circleClipWidth, circleHeight, circleshiftX, newCircleSpritePosi, circlePrintWidth, circlePrintHeight);
    
    if(newCircleSpritePosi >= drummerHeight-circleHeight){
        newCircleSpritePosi = 0;
        context.clearRect(0, 0, canvas.width, canvas.height);
        direction = Math.random();
        lives--;
    }

}

function circleFromDown(circleshiftX, circleSpritePosi){
    
    downcircleSpritePosi = downcircleSpritePosi - 10 *(animateVelocity);

    context.drawImage(spritesheet, circleClipX, circleClipY, circleClipWidth, circleHeight, circleshiftX, downcircleSpritePosi, circlePrintWidth, circlePrintHeight);



    if(downcircleSpritePosi <= drummerHeight+drummerPrintHeight){
        downcircleSpritePosi = 0;
        context.clearRect(0, 0, canvas.width, canvas.height);
        direction = Math.random();
        lives--;
    }

}

function circleFromLeft(circleshiftY, circleSpritePosi){

    
    newCircleSpritePosi = newCircleSpritePosi -(circleSpritePosi* (animateVelocity));
    context.drawImage(spritesheet, circleClipX, circleClipY, circleClipWidth, circleHeight, newCircleSpritePosi, circleshiftY-30, circlePrintWidth, circlePrintHeight);

    
    if(newCircleSpritePosi > drummerSpritePosi-85){
        newCircleSpritePosi = -90;
        context.clearRect(0, 0, canvas.width, canvas.height);
        direction = Math.random();
        lives--;
    }
    

}

function circleFromRight(circleshiftY, circleSpritePosi){
    
    
    rightcircleSpritePosi = rightcircleSpritePosi - circleSpritePosi*(animateVelocity);

    context.drawImage(spritesheet, circleClipX, circleClipY, circleClipWidth, circleHeight, rightcircleSpritePosi, circleshiftY -30, circlePrintWidth, circlePrintHeight);
    


    if(rightcircleSpritePosi <= drummerSpritePosi + 165){
        rightcircleSpritePosi = 0;
        context.clearRect(0, 0, canvas.width, canvas.height);
        direction = Math.random();
        lives--;
    }

}

function drawDrummer(){
    if (drummer == null){
        context.drawImage(spritesheet, drummerNeutralClipX, drummerNeutralClipY, drummerNeutralClipWidth, drummerHeight, drummerSpritePosi, 220, drummerPrintWidth, drummerPrintHeight);
    }

    else if(drummer == 0){
        drumLeft();
    }

    else if(drummer == 1){
        drumRight();
    }

    else if(drummer == 2){
        drumUp();
    }

    else if(drummer == 3){
        drumDown();
    }

}

function drawCircle(){
    context.drawImage(spritesheet, circleClipX, circleClipY, circleClipWidth, circleHeight, 0,0,circlePrintWidth, circlePrintHeight);
}

function drawBackground(){
    context.drawImage(background, 0, 0, canvas.width, canvas.height);
}

function drumLeft(){
    if (paused == false && failed == false) {
        context.clearRect(0, 0, canvas.width, canvas.height);
        drawBackground();
        context.drawImage(spritesheet, drummerRightClipX, drummerRightClipY, drummerRightClipWidth, drummerHeight, drummerSpritePosi, 220, drummerPrintWidth, drummerPrintHeight);
    }
}

function drumRight(){
    if (paused == false && failed == false) {
        context.clearRect(0, 0, canvas.width, canvas.height);
        drawBackground();
        context.drawImage(spritesheet, drummerLeftClipX, drummerLeftClipY, drummerLeftClipWidth, drummerHeight, drummerSpritePosi, 220, drummerPrintWidth, drummerPrintHeight);
    }
}

function drumUp(){
    if (paused == false && failed == false) {
        context.clearRect(0, 0, canvas.width, canvas.height);
        drawBackground();
        context.drawImage(spritesheet, drummerUpClipX, drummerUpClipY, drummerUpClipWidth, drummerHeight, drummerSpritePosi, 220, drummerPrintWidth-10, drummerPrintHeight);
    }
}

function drumDown(){

    if (paused == false && failed == false) {
        context.clearRect(0, 0, canvas.width, canvas.height);
        drawBackground();
        context.drawImage(spritesheet, drummerDownClipX, drummerDownClipY, drummerDownClipWidth, drummerHeight, drummerSpritePosi, 220, drummerPrintWidth-10, drummerPrintHeight);
    }

}

function detectBeatTouch(){

    if(drummer == 0 && newCircleSpritePosi < drummerSpritePosi - 85){
        context.clearRect(newCircleSpritePosi, circleshiftY, 90,90);
        newCircleSpritePosi= -90;
        direction = Math.random();
        score +=1;
    }

    else if (drummer == 1 && rightcircleSpritePosi > (drummerSpritePosi + 165)){
        context.clearRect(rightcircleSpritePosi, circleshiftY, 90, 90);
        rightcircleSpritePosi = 0;
        direction = Math.random();
        score +=1;
    }

    else if (drummer == 2 && newCircleSpritePosi){
        context.clearRect(newCircleSpritePosi, circleshiftX, 90, 90);
        newCircleSpritePosi = -90;
        direction = Math.random();
        score +=1;
    }

    else if (drummer == 3 && downcircleSpritePosi){
        context.clearRect(downcircleSpritePosi, circleshiftX, 90,90);
        downcircleSpritePosi = canvas.height + 10;
        direction = Math.random();
        score +=1;
    }

    speedUp();

    document.getElementById('score').innerHTML = 'Score: ' + score;
    document.getElementById('lives').innerHTML = 'Lives: ' + lives;

}

function gameOver(){

    
    unpause();
    paused != true;
    failed = true;

    console.log("rip");


    context.fillText("Game Over! Tap the Screen to retry", 15, canvas.height / 2);
    musicElement.stop();

}


function startMusic(){
    musicElement.play();
}

function restartGame(){
    context.clearRect(0,0, canvas.width, canvas.height);

//drummer
drummerSpritePosi = 90;
drummerHeight = 251;
drummerPrintWidth=170;
drummerPrintHeight=220;

drummerDownClipX=0;
drummerDownClipY=0;
drummerDownClipWidth=137;


drummerRightClipX=137;
drummerRightClipY=0;
drummerRightClipWidth=135;

drummerUpClipX=272;
drummerUpClipY=0;
drummerUpClipWidth=142;

drummerNeutralClipX=0;
drummerNeutralClipY=255;
drummerNeutralClipWidth=140;

drummerLeftClipX=144;
drummerLeftClipY=256;
drummerLeftClipWidth=131;


//circles
circleSpritePosi=0;
newCircleSpritePosi=0;

circleHeight=115;
circlePrintHeight=90;
circlePrintWidth=90;

circleshiftX = 0;
circleshiftY = 0;


circleClipX = 320;
circleClipY = 269;
circleClipWidth = 109;

count = 0;


direction = Math.random();

animateVelocity = 0.3;

rightcircleSpritePosi = canvas.width + 10;
downcircleSpritePosi = canvas.height + 10;

lives = 3;


failed = false;
paused = false;

score = 0;

}

function mousechecker(canvas, evt){

        var rect = canvas.getBoundingClientRect();
        return {
          x: evt.clientX - rect.left,
          y: evt.clientY - rect.top
        };
        console.log(evt.clientX, evt.clientY);
    }


//function to store the score in local storage doent appear to work
function setScore() {
    localStorage.setItem("Highscore", highscore);
}

//function to retrieve the score from local storage and place within the highscore vlaue doent appear to work
function getScore() {
    document.getElementById('Highscore').innerHTML = "Highscore: " + localStorage.getItem("Highscore");
}


A_KEY = 65;
LEFT_ARROW = 37;
D_KEY = 68;
RIGHT_ARROW = 39;

UP_ARROW = 38;
W_KEY = 87;

DOWN_ARROW= 40;
S_KEY = 83;

P_KEY = 80;
R_KEY = 82;

O_KEY = 79;


canvas.addEventListener("click", function (evt) {
    var mousePos = mousechecker(canvas, evt);
    
    if (mousePos.x < 130 && mousePos.y > 250 && mousePos.y < 435){
        drummer = 0;
    }

    else if (mousePos.x > 275 && mousePos.y > 250 && mousePos.y < 435){
       drummer =1;
    }

    
    else if(mousePos.x > 130 && mousePos.x < 280 && mousePos.y < 210){
        drummer =2;
    }

    else if (mousePos.x > 130 && mousePos.x < 280 && mousePos.y > 440){
       drummer = 3;
    }

    if(failed == true && mousePos.x >=0 && mousePos.y >=0){
        if (failed == true) {
            failed = false;
            unpause();

            restartGame();

            initializeImages();
        }
    }
    
},
false);


window.addEventListener('keydown', function (e) {
    var key = e.keyCode;

    if (key === A_KEY || key === LEFT_ARROW) {
        
        drummer = 0;
    }

    else if (key === D_KEY || key === RIGHT_ARROW) {
        
        drummer = 1
    }

    else if (key === W_KEY || key === UP_ARROW){
       
        drummer = 2;
    }

    else if (key === S_KEY || key === DOWN_ARROW){
        
        drummer = 3;
    }

    else if (key === P_KEY && failed == false) {
        pause();
        console.log(paused);
    }

    else if (key === O_KEY && failed == false) {

        //set to unpause the gam only if it is alrady paused
        if (paused === true) {
            context.clearRect(0, -canvas.height, canvas.width, canvas.height * 3);
            unpause();
            initializeImages();
            console.log(paused);
        }
    }

    //prevents the game from being restarted while paused mainly to prevent glitching
    else if (key === R_KEY && paused === false) {

        restartGame();

        if (failed == true) {
            failed = false;
            unpause();

            initializeImages();
        }

    }

});


initializeImages();
